﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5092_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Artist> AllArtists = MakeExampleArtistList();
            List<ArtWork> AllWork = MakeExampleArtWorkList(AllArtists);

            // these two queries are identical.  the first uses query language and the second uses extension methods/lambda
            var q = from a in AllArtists
                    where a.LastName == "Degas"
                    select a;

            var q_em = AllArtists.Where(x => x.LastName == "Degas");

            // here we demonstrate the use of a join statement in LINQ
            var q2 = from a in AllArtists
                     join w in AllWork
                     on a.LastName equals w.CreatorLastName
                     where a.FirstName == "Pablo"
                     select new { FN = a.FirstName, WN = w.NameOfWork };

            // here we show how to order all the arists
            var q3 = from a in AllArtists
                     orderby a.LastName
                     select a;

            // a simple grouping is performed here
            var q4 = from w in AllWork
                     group w by w.CreatorLastName into NameGroup
                     select new { Name = NameGroup.Key };

            // a grouping statement with a sum logic
            var q5 = from w in AllWork
                     group w by w.CreatorLastName into G
                     select new { Name = G.Key, Value = G.Sum(x => x.MarketValue) };

            foreach(var g in q)
            {
                Console.WriteLine(g.FirstName + " " + g.LastName + " Age:" + g.Age.ToString());
            }

            Console.WriteLine();

            foreach(var g in q2)
            {
                Console.WriteLine(g.FN + " " + g.WN);
            }

            Console.WriteLine();

            foreach(var g in q3.Reverse())
            {
                Console.WriteLine(g.LastName);
            }

            Console.WriteLine();

            foreach(var g in q4)
            {
                Console.WriteLine(g.Name);
            }

            Console.WriteLine();

            foreach(var g in q5)
            {
                Console.WriteLine(g.Name + " Total Art Value: $" + g.Value.ToString());
            }

            Console.ReadLine();

        }

        static List<ArtWork> MakeExampleArtWorkList(List<Artist> artists)
        {
            var matisse = from a in artists
                          where a.LastName == "Matisse"
                          select a;

            var picasso = from a in artists
                          where a.LastName == "Picasso"
                          select a;

            var degas = from a in artists
                        where a.LastName == "Degas"
                        select a;

            return new List<ArtWork>
            {
                new ArtWork()
                {
                    Creator = matisse.First(),
                    CreatorLastName = matisse.First().LastName,
                    NameOfWork = "Reclining Nude I (Dawn)",
                    MarketValue = 9200000
                },
                new ArtWork()
                {
                    Creator = matisse.First(),
                    CreatorLastName = matisse.First().LastName,
                    NameOfWork = "The Plum Blossoms",
                    MarketValue = 25000000
                },
                new ArtWork()
                {
                    Creator = picasso.First(),
                    CreatorLastName = picasso.First().LastName,
                    NameOfWork = "Garcon a la Pipe",
                    MarketValue = 104100000
                },
                new ArtWork()
                {
                    Creator = picasso.First(),
                    CreatorLastName = picasso.First().LastName,
                    NameOfWork = "Dora Maar with Cat",
                    MarketValue = 95200000
                },
                new ArtWork()
                {
                    Creator = degas.First(),
                    CreatorLastName = degas.First().LastName,
                    NameOfWork = "Portrait in White",
                    MarketValue = 17000
                },
                new ArtWork()
                {
                    Creator = degas.First(),
                    CreatorLastName = degas.First().LastName,
                    NameOfWork = "Dancers in Green Room",
                    MarketValue = 10500
                }
            };
        }

        static List<Artist> MakeExampleArtistList()
        {
            return new List<Artist>
            {
                new Artist()
                {
                    FirstName = "Henri",
                    LastName = "Matisse",
                    BirthDate = new DateTime(1869, 12, 31),
                    Age = 84
                },
                new Artist()
                {
                    FirstName = "Pablo",
                    LastName = "Picasso",
                    BirthDate = new DateTime(1881, 10, 25),
                    Age = 91
                },
                new Artist()
                {
                    FirstName = "Edgar",
                    LastName = "Degas",
                    BirthDate = new DateTime(1834, 7, 19),
                    Age = 83
                }
            };
        }
    }

    public class Artist
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age { get; set; }
    }

    public class ArtWork
    {
        public Artist Creator { get; set; }
        public string CreatorLastName { get; set; }
        public string NameOfWork { get; set; }
        public double MarketValue { get; set; }
    }
}
